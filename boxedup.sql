-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2018 at 04:01 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `boxedup`
--

-- --------------------------------------------------------

--
-- Table structure for table `box_properties`
--

CREATE TABLE `box_properties` (
  `id` int(11) NOT NULL,
  `box_name` varchar(255) NOT NULL,
  `front_img` varchar(10000) NOT NULL,
  `right_img` varchar(10000) NOT NULL,
  `left_img` varchar(10000) NOT NULL,
  `back_img` varchar(10000) NOT NULL,
  `top_img` varchar(10000) NOT NULL,
  `bottom_img` varchar(10000) NOT NULL,
  `front_bg` varchar(255) NOT NULL,
  `left_bg` varchar(255) NOT NULL,
  `right_bg` varchar(255) NOT NULL,
  `back_bg` varchar(255) NOT NULL,
  `top_bg` varchar(255) NOT NULL,
  `bottom_bg` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `box_properties`
--

INSERT INTO `box_properties` (`id`, `box_name`, `front_img`, `right_img`, `left_img`, `back_img`, `top_img`, `bottom_img`, `front_bg`, `left_bg`, `right_bg`, `back_bg`, `top_bg`, `bottom_bg`) VALUES
(29, 'ee', '../assets/img/app/3d4efd55c93fef5a133ef5e1c994c4fc.png,width: 62.639%; height: 59.0249%; left: 16.0905%; top: 18.7901%;', '', '', '', '', '', 'rgb(255, 128, 128)', 'rgb(255, 128, 128)', 'rgb(255, 255, 255)', 'rgb(255, 128, 128)', 'rgb(255, 128, 128)', 'rgb(255, 255, 255)'),
(30, 'ee', '../assets/img/app/favicon.ico,width: 73.207%; height: 68.3707%; left: 18.5094%; top: 579.178%;', '', '', '', '', '', 'rgb(128, 255, 128)', 'rgb(128, 255, 128)', 'rgb(255, 255, 255)', 'rgb(255, 255, 255)', 'rgb(255, 255, 255)', 'rgb(255, 255, 255)'),
(31, 'ew', '../assets/img/app/3d4efd55c93fef5a133ef5e1c994c4fc.png,width: 44.8984%; height: 41.9452%; left: 31.1763%; top: 19.629%;', '', '', '', '', '', 'rgb(0, 128, 0)', 'rgb(0, 128, 0)', 'rgb(255, 255, 255)', 'rgb(128, 255, 128)', 'rgb(255, 255, 255)', 'rgb(255, 255, 255)'),
(32, 'eq', '../assets/img/app/solutions.png,width: 44.8984%; height: 41.9452%; left: 24.711%; top: 16.6929%;', '', '', '', '', '', 'rgb(0, 128, 0)', 'rgb(0, 128, 0)', 'rgb(255, 255, 255)', 'rgb(128, 255, 128)', 'rgb(255, 255, 255)', 'rgb(255, 255, 255)'),
(33, 'ww', '../assets/img/app/template-logo.png,width: 44.8984%; height: 41.9452%; left: 24.711%; top: 16.6929%;', '', '', '', '', '', 'rgb(0, 128, 0)', 'rgb(0, 128, 0)', 'rgb(255, 255, 255)', 'rgb(128, 255, 128)', 'rgb(255, 255, 255)', 'rgb(255, 255, 255)'),
(34, '222', '../assets/img/app/template-logo.png,width: 99.9945%; height: 45.6154%; left: -2.22808%; top: 12.9178%;', '', '', '', '', '', 'rgb(128, 255, 255)', 'rgb(128, 255, 255)', 'rgb(255, 255, 255)', 'rgb(0, 128, 0)', 'rgb(255, 255, 255)', 'rgb(255, 255, 255)'),
(35, 'qweqwe', '../assets/img/app/3d4efd55c93fef5a133ef5e1c994c4fc.png,width: 82.5459%; height: 60.8206%; left: 0%; top: 0%;', '', '', '', '', '', 'rgb(255, 255, 255)', 'rgb(255, 255, 255)', 'rgb(255, 255, 255)', 'rgb(255, 255, 255)', 'rgb(255, 255, 255)', 'rgb(255, 255, 255)');

-- --------------------------------------------------------

--
-- Table structure for table `saved_boxes`
--

CREATE TABLE `saved_boxes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `box_name` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` float NOT NULL,
  `property_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saved_boxes`
--

INSERT INTO `saved_boxes` (`id`, `user_id`, `box_name`, `quantity`, `unit_price`, `property_id`) VALUES
(14, 16, 'ee', 100, 3.9, 29),
(15, 17, 'ee', 20, 6, 30),
(16, 17, 'ew', 20, 6, 31),
(17, 17, 'eq', 20, 6, 32),
(18, 17, 'ww', 20, 6, 33),
(19, 17, '222', 20, 6, 34),
(20, 16, 'qweqwe', 20, 6, 35);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(40) NOT NULL,
  `email` varchar(255) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `firstName`, `lastName`) VALUES
(12, 'superuser', '8e67bb26b358e2ed20fe552ed6fb832f397a507d', 'user@gmail.com', 'Super', 'User'),
(16, 'jengkarlong', '601f1889667efaebb33b8c12572835da3f027f78', 'jen@gmail.com', 'Jen', 'Villaganas'),
(17, 'user1', '601f1889667efaebb33b8c12572835da3f027f78', 'user1@gam.c', 'user1', 'user1'),
(18, 'user2', '601f1889667efaebb33b8c12572835da3f027f78', 'user2@gasad.c', 'user2', 'user2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `box_properties`
--
ALTER TABLE `box_properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `saved_boxes`
--
ALTER TABLE `saved_boxes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `property_id` (`property_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `box_properties`
--
ALTER TABLE `box_properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `saved_boxes`
--
ALTER TABLE `saved_boxes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `saved_boxes`
--
ALTER TABLE `saved_boxes`
  ADD CONSTRAINT `saved_boxes_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `saved_boxes_ibfk_2` FOREIGN KEY (`property_id`) REFERENCES `box_properties` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
