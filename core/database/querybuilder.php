<?php


class QueryBuilder
{
  protected $pdo;

// pass pdo connection
  public function __construct($pdo)
  {
    $this->pdo = $pdo;
  }

//show all -> no use -_-
  public function selectAll($table)
  {
    $statement = $this->pdo->prepare("SELECT * FROM $table");

    $statement->execute();
    
    return $statement->fetchAll(PDO::FETCH_ASSOC);
  }

//create new user
  public function insertNewUser($firstName, $lastName, $username, $password,$email)
  {
    $statement = $this->pdo->prepare(
      "INSERT INTO users (username, password, email, firstName, lastName)
        VALUES( '$username', '$password', '$email', '$firstName', '$lastName')"
      );
    $statement->execute();
  }

//validation
  public function checkIfExist($table, $key, $value)
  {

    $statement = $this->pdo->prepare(
      "SELECT * FROM $table WHERE $key = '$value'"
    );
    $statement->execute();

    return $statement->fetchAll(PDO::FETCH_ASSOC);
  }
  public function saveNewBox($event, $pty_id, $user_id, $boxname, $qty, $price, $boxtype)
  {
    if($event == 'insert') {
      $statement = $this->pdo->prepare(
        "INSERT INTO saved_boxes(property_id, user_id, box_name, quantity, unit_price, box_type)
          VALUES('$pty_id', $user_id, '$boxname', $qty, $price, '$boxtype')"
      );
    }else if ($event == 'update') {
      $statement = $this->pdo->prepare(
        "UPDATE saved_boxes SET quantity = $qty, unit_price = $price, box_type = '$boxtype' WHERE user_id = $user_id AND box_name = '$boxname'"
      );
    }
    $statement->execute();
  }

  public function boxProperties($event, $pty_id ,$boxname, $front, $left, $right, $back, $top, $bot, $frontBG, $leftBG, $rightBG, $backBG, $topBG, $botBG)
  {
    if($event == 'insert') {
      $statement = $this->pdo->prepare(
        "INSERT INTO box_properties(
          box_name, front_img, left_img, right_img, back_img, top_img, bottom_img, front_bg, left_bg, right_bg, back_bg, top_bg, bottom_bg)
          VALUES('$boxname', '$front', '$left', '$right', '$back', '$top', '$bot', '$frontBG', '$leftBG', '$rightBG', '$backBG', '$topBG', '$botBG')
        ");
    }else if ($event = 'update') {
      $statement = $this->pdo->prepare(
        "UPDATE box_properties SET front_img = '$front', left_img = '$left', right_img = '$right', back_img = '$back', top_img = '$top', bottom_img = '$bot', front_bg = '$frontBG', left_bg = '$leftBG', right_bg= '$rightBG', back_bg = '$backBG', top_bg = '$topBG', bottom_bg = '$botBG' WHERE box_name = '$boxname' AND id = $pty_id;"
      );
    }
    $statement->execute();
  }

  public function fetchBox($userid, $boxname)
  {
    $statement = $this->pdo->prepare(
      "SELECT * FROM saved_boxes WHERE user_id = $userid AND box_name = '$boxname'"
    );
    $statement->execute();
    return $statement->fetchAll(PDO::FETCH_ASSOC);
  }

  public function deleteBox($table, $key, $value) 
  {
    
      $statement = $this->pdo->prepare(
        "DELETE FROM $table WHERE $key = $value"
      );
    $statement->execute();
  }


  public function modifyOrders($event, $userid, $boxid, $qty, $price, $total, $odate)
  {
    if( $event == 'insert') {
      $statement = $this->pdo->prepare(
        "INSERT INTO orders(user_id, box_id, quantity, unit_price, total, ordered_date) VALUES($userid, $boxid, $qty, $price, $total, '$odate')"
      );
      $statement->execute();
    }
  }

  public function changeStatus($table, $key, $value, $id)
  {
    
    $statement = $this->pdo->prepare(
      "UPDATE $table SET $key = '$value' WHERE id = $id"
    );
    $statement->execute();

  }




  

}

