<?php

$app['config'] = require 'config.php';


require 'core/router.php';
require 'core/request.php';
require 'database/connection.php';
require 'database/querybuilder.php';


$app['database'] =  new QueryBuilder(
  Connection::make($app['config']['database'])
);