$(document).ready(function() {
 var path = window.location.href;
 if( path == 'http://localhost:8888/profile#savedboxes') {
  
  $.ajax({
    method: 'GET',
    url: '/myboxes',
    success: function(data) {
      $('#main-view').html(data);
      $('#myboxes').addClass('selected');
      $('#myboxes').siblings().removeClass('selected');
    }
  });
 }else if (path == 'http://localhost:8888/profile#cart') {
  
 $.ajax({
     method: 'GET',
     url: 'views/partials/cart.php',
     success: function(data) {
       $('#main-view').html(data);
       $('#see-cart').addClass('selected');
       $('#see-cart').siblings().removeClass('selected');
     }
   })
 }else if (path == 'http://localhost:8888/profile#orders') {
   
  $.ajax({
    method: 'GET',
    url: '/orders',
    success: function(data) {
      $('#main-view').html(data)
       $('#order-status').addClass('selected');
       $('#order-status').siblings().removeClass('selected');
    }
  });
  }else if (path == 'http://localhost:8888/profile#users') {
    $.ajax({
      method: 'GET',
      url: '/manage-user',
      success: function(data) {
        $('#main-view').html(data);
        $('#manage-users').addClass('selected');
        $('#manage-users').siblings().removeClass('selected');
        
      }
    });
  }else if (path == 'http://localhost:8888/profile') {
   
  $.ajax({
    method: 'post',
    url: 'views/partials/account.php',
    success: function(data) {
      $('#main-view').html(data)

    }
   
  });
  }


});


$('#profile-nav').on('click', 'li', function() {
  $(this).addClass('selected');
  $(this).siblings().removeClass('selected');
});

$('#details').on('click', function() {
  $.ajax({
    method: 'post',
    url: 'views/partials/account.php',
    success: function(data) {
      $('#main-view').html(data)
    }
  });
});

$('#myboxes').on('click', function() {

$.ajax({
  method: 'GET',
  url: '/myboxes',
  success: function(data) {
    $('#main-view').html(data);
  }
})

});

$('#see-cart').on('click', function() {
 $.ajax({
     method: 'GET',
     url: 'views/partials/cart.php',
     success: function(data) {
       $('#main-view').html(data);
     }
   })
});

$('#order-status').on('click', function() {
  $.ajax({
    method: 'GET',
    url: '/orders',
    success: function(data) {
      $('#main-view').html(data)

    }
  });
});

$('#manage-users').on('click', function() {
  $.ajax({
    method: 'GET',
    url: '/manage-user',
    success: function(data) {
      $('#main-view').html(data);
    }
  });
});

//order
