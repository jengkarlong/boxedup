$(document).ready(function() {

  $('.preview-img').resizable({
    ghost: true
  });
  $('.img-drag').draggable();
  $('#colorpick').val('#ffffff');
  $('#box-quantity').val($('#prev-qty').html());

  $('.total').html(parseFloat($('.uprice').html())*$('#box-quantity').val());

  //will load if user clicked customize btn
  $('.preview').css('background-color',$('box.active').css('background-color'));
  if($('box.active .face-img').attr('src')){
    $('.preview-img').attr('src', $('box.active .face-img').attr('src') );
    $('.img-drag').css('opacity', '1');
  }


});
var boxObj = new Object();
var qty = 6;
//view function
var target = 'front';
$('.view-ul li').on('click', function() {
  target = $(this).html();
    $('#colorpick').val('#ffffff');
    $(this).addClass('active');
    $(this).siblings().removeClass('active');
    $('.box-wrapper').attr('id',target+'-anim');
    $('.preview-window').attr('id',target+'-size')
    $('.'+target).addClass('active');
    $('.'+target).siblings().removeClass('active');    
    $('.preview-img').attr('src','' );
    $('.img-drag').css('opacity', 0);
    $('.preview').css('background-color',$('box.active').css('background-color'));
    if($('box.active .face-img').attr('src')){
       $('.preview-img').attr('src', $('box.active .face-img').attr('src') );
       $('.img-drag').css('opacity', '1');
    setTimeout(function() {
    $('.img-drag').css({
      'width': ($('box.active .face-img').width() / $('box.active').width())*100+'%',
      'height': ($('box.active .face-img').height()/$('box.active').height())*100+'%',
      'left': (100*$('box.active .face-img').position().left)/$('box.active').width()+'%',
      'top': (100*$('box.active .face-img').position().top)/$('box.active').height()+'%'
    });},500);
    }

    $('.preview-img').attr('src', $('box.active .face-img').attr('src') );
    $('.preview').css('background-color', $('box.active').css('background-color'));
});

$('#colorpick').on('change', function() {
  var newbg = $(this).val();
  $('.preview').css('background-color', newbg);
  $('box.active').css('background-color', newbg);
});

$('#new-art').on('change', function() {
var file_data = $(this).prop('files')[0];   
var form_data = new FormData();                  
  form_data.append('file', file_data);
  $.ajax({
      url: "controllers/upload_files.php",
      type: "POST",
      data:  form_data,
      contentType: false,
      cache: false,
      processData:false,
      success: function(data){
   
         
          if(data != ''){
            $('box.active').append('<img data-index="'+$('.view-ul .active').index()+'" src="'+data+'" class="face-img">');   
            $('.preview-img').attr('src', $('box.active .face-img').attr('src'));
            $('.img-drag').css('opacity', '1');
            $('box.active .face-img').css({
            'width': ($('.preview-img').width() / $('.preview-window').width())*100+'%',
            'height': ($('.preview-img').height()/$('.preview-window').height())*100+'%',
            'left': (100*$('.img-drag').position().left)/$('.preview-window').width()+'%',
            'top': (100*$('.img-drag').position().top)/$('.preview-window').height()+'%'
            });
          }
      }
  });
});

$('.preview').on('mousemove', function() {
  $('box.active .face-img').css({
    'width': ($('.preview-img').width() / $('.preview-window').width())*100+'%',
    'height': ($('.preview-img').height()/$('.preview-window').height())*100+'%',
    'left': (100*$('.img-drag').position().left)/$('.preview-window').width()+'%',
    'top': (100*$('.img-drag').position().top)/$('.preview-window').height()+'%'
  });
         
  
})

$('.remove-img').on('click', function() {
  $('box.active img').remove();
  $('.preview-img').attr('src', '');
  $('.img-drag').css('opacity', 0);
});


$('.boxz').on('click', function() {
  $(this).addClass('selected');
  $(this).siblings().removeClass('selected');
  
});

//Quantity and Unit Price
$('#box-quantity').on('change', function() {
  var selected = $(this).val();
  if( selected == 20) {
    $('.untprice').html('$'+6)
    qty = 6;
  }else if( selected == 50) {
    $('.untprice').html('$'+4.3)
    qty = 4.30;
  }else if ( selected == 100) {
    $('.untprice').html('$'+3.9)
    qty = 3.90;
  }

$('.total').html(qty*selected);

});



$('#save-design').on('click', function() {
  $('.name-popup').css('display', 'block')
});
$('#close-name').on('click', function() {
  $('.name-popup').css('display', 'none')
});


//Save Box Properties to JS OBJECT
$('#save-name').on('click', function() {
  
  $('.name-popup').css('display', 'none')
  boxObj.boxname = $('#boxname').val().toLowerCase();
  boxObj.frontBG = $(' .front').css('background-color');
  boxObj.leftBG = $(' .left').css('background-color');
  boxObj.rightBG = $(' .right').css('background-color');
  boxObj.backBG = $(' .back').css('background-color');
  boxObj.topBG = $(' .top').css('background-color');
  boxObj.bottomBG = $(' .bottom').css('background-color');
  boxObj.qty = parseInt($('#box-quantity').val());
  boxObj.unitPrice = parseFloat(qty);
  boxObj.boxType = $('.box-sizing .selected span').html();
  if($('.front > img').attr('src')) {
    boxObj.frontImage = {
      src: $('.front img').attr('src'),
      css: $('.front img').attr('style') }
  }else {
    boxObj.frontImage = '';
  }
  if($('.left > img').attr('src')) {
    boxObj.leftImage = {
      src: $('.left img').attr('src'),
      css: $('.left img').attr('style') }
  }else {
     boxObj.leftImage = '';
  }
  if($('.right > img').attr('src')) {
    boxObj.rightImage = {
      src: $('.right img').attr('src'),
      css: $('.right img').attr('style') }
  }else {
    boxObj.rightImage = '';
  }
  if($('.back > img').attr('src')) {
    boxObj.backImage = {
      src: $('.back img').attr('src'),
      css: $('.back img').attr('style') }
  } else {
     boxObj.backImage = '';
  }
  if($('.top > img').attr('src')) {
    boxObj.topImage = {
      src: $('.top img').attr('src'),
      css: $('.top img').attr('style') }
  }else {
     boxObj.topImage = '';
  }
  if($('.bottom > img').attr('src')) {
    boxObj.bottomImage = {
      src: $('.bottom img').attr('src'),
      css: $('.bottom img').attr('style') }
  }else {
    boxObj.bottomImage = '';
  }
var newJson= JSON.stringify(boxObj)

$.ajax({
  metod: 'GET',
  url: '/savebox',
  data: {
    json: newJson
  },
  success: function(data) {
    console.log(data);
  }
});

});


$('.main-bgs div').on('click', function() {
var newbg = $(this).css('background-color');

  $('main').css('background-color', newbg);
  $('.copybg').css('background-color', newbg);
});

$('#open-left').on('click', function() {
  $(this).next().toggleClass('left-slide')
});
$('#open-right').on('click', function() {
  $(this).next().toggleClass('right-slide')
 
});

