


$(window).scroll(function() {
    var winScroll = $(this).scrollTop();

// && $(window).width() > 770
if(winScroll < $(window).height() && $(window).width() > 1024 ) {
  $('.box-container').css({
  'position': 'fixed', 
  'top': 110+'px' 
  });
  $('.box1').css({
    'transform': 'translate('+ winScroll/2.3 +'px,'+ winScroll/9+'px)'
  });
  $('.box2').css({
    'transform': 'translate('+ winScroll/6 +'px,'+ winScroll/10 +'px)'
  });
   $('.box3').css({
    'transform': 'translate('+ 0 +'px,'+ winScroll/25+'px)'
  });
  $('.box-type').css({
    'opacity': 0
  });
}else if (winScroll > $(window).height() && $(window).width() > 1024 ){
  $('.box-container').css({
    'position': 'relative',
    'top': $(window).height()+110+'px'
  });
  $('.box-type').css({
    'opacity': 1
  });
}

if(winScroll > $('#how-to').offset().top - $(window).height() * 0.2){
   $('.step').each(function(i) {
     $('.step').eq(i).css({
       'animation': 'showStep 0.5s ease-in-out forwards',
       'animation-delay': i*0.3+'s'
     })
   });
}

});

$(window).on('mousemove', function(event) {


  $('.stripes').css({
    'transform': 'translate(-'+event.clientX/55+'px,-'+event.clientY/60+'px)'
  });

});
