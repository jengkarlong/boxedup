<?php


if(!isset($_SESSION['user'])) {
  header('location: /#register');
}
if(isset($_SESSION['box'])) {
  $box = $_SESSION['box'];
  $pty = $_SESSION['pty'];
  require 'partials/boxvariables.php';

}
require 'partials/get.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>BOXEDUP | Designer</title>
  <?php get_head(); ?>
  <link rel="stylesheet" href="assets/css/app.css">
</head>
<body>
<?php get_header();
?>

<main>

<button class='menu-btn' id='open-left'><i class='fa fa-sliders' style='font-size: 18px'> </i></button>
<div id="left-container" class='menu-container'>
  <div class="box-sizing">
  <h2>Choose Box Size</h2>
    <div class="shipping boxz selected">
      <span>Shipping</span>
    </div>
    <div class="rigid boxz">
      <span>Rigid</span>
    </div>
    <div class="folding boxz">
      <span>Folding</span>
    </div>
  </div>
  <div class="box-pricing">
  <h2 class='qty'>Quantity</h2>
  <h2 class='cost'>Unit Price</h2>
    <div  class='untprice'>$<span class='uprice'><?= isset($box['unit_price']) ? $box['unit_price'] : 6 ?></span></div>
    <span id="prev-qty" hidden><?= isset($box['quantity']) ? $box['quantity'] : 20 ?></span>
    <select name="fr-quantity" id="box-quantity">
      <option value="20">20</option>
      <option value="50">50</option>
      <option value="100">100</option>
    </select>
    <h2 class='total-text'>Total: <span style='color:#0F82FD '>$</span><span class='total'></span></h2>
  </div>
</div>

<div id="preview-container">
  <div id="perspective">
    <div class="box-wrapper shipping-box" id='front-anim'>
    <?php if(isset($_SESSION['box'])) : ?>
      <box class="front active" style='background-color: <?= $frontbg ?>'>
      <?= $front ? "<img src='$front' style='$frontcss' class='face-img'>"  : '' ?>
      </box>
      <box class="left" style='background-color: <?= $leftbg ?>'>
      <?= $left ? "<img src='$left' style='$leftcss' class='face-img'>"  : '' ?>
      </box>
      <box class="right" style='background-color: <?= $rightbg ?>'>
      <?= $right ? "<img src='$right' style='$rightcss' class='face-img'>"  : '' ?>
      </box>
      <box class="back" style='background-color: <?= $backbg ?>'>
      <?= $back ? "<img src='$back' style='$backcss' class='face-img'>"  : '' ?>
      </box>
      <box class="top" style='background-color: <?= $topbg ?>'>
      <?= $top ? "<img src='$top' style='$topcss' class='face-img'>"  : '' ?>
      </box>
      <box class="bottom" style='background-color: <?= $bottombg ?>'>
     <?= $bottom ? "<img src='$bottom' style='$bottomcss' class='face-img'>"  : '' ?> 
      </box>
    <?php else: ?>
     <box class="front active"></box>
     <box class="left"></box>
     <box class="right"></box>
     <box class="back"></box>
     <box class="top"></box>
     <box class="bottom"></box>
    <?php endif; ?>
    </div>
  </div>
</div>

<button class='menu-btn'  id="open-right"><i class='fa fa-cogs' style='font-size: 18px'></i></button>
<div id="right-container" class='menu-container'>
  <div class="face-option">
    <h2>VIEW</h2>
    <ul class='view-ul'>
      <li class='frt active'>front</li>
      <li class='lft'>left</li>
      <li class='rgt'>right</li>
      <li class='bck'>back</li>
      <li class='tpp'>top</li>
      <li class='btm'>bottom</li>
    </ul>
  </div>
  <div class="add-art">
    <button class='upload-art'><i class='fa fa-paint-brush'> </i> ADD ART
      <input name="myFile" type="file" name='image' accept="image/*" id='new-art'>
    </button>
  </div>
  <div class="add-text">
    <button id="new-text">
     <i class='fa fa-font'> </i> ADD TEXT
    </button>
  </div>
  <div class="fill-background">
    <span>FILL BACKGROUND</span>
    <input type="color" id='colorpick' name='fr-bgcolor'>
  </div>
  <div class="preview">
    <div class="preview-window" id='front-size'>
      <div class="img-drag" style='display: inline-block; opacity: 0;'>
        <img src="<?= isset($front) ? $front : '' ?>" alt="preview" class='preview-img' >
      </div>
    </div>
    <button class='remove-img'><i class="fa fa-trash-o"></i></button>
    
  </div>
</div>

<button id='save-design'>SAVE</button>
<div id='main-bg'>
  <div class="selected-main-bg">
    <img src="assets/img/bucket.png" class='buck-icon' alt='bucket icon' width='100'>
    <span class="copybg"> </span>
    <span>BACKGROUND</span>
  </div>
  <div class="main-bgs" >
    <div class='main-gray'></div>
    <div class="main-blue"></div>
    <div class="main-red"></div>
    <div class="main-dark"></div>
    <div class="main-yellow"></div>
  </div>
</div>

<div class="name-popup">
  <span id='close-name'>X</span>
  <span>and your box name is?</span>
  <input type="tpye" name='boxname' id='boxname' value="<?= isset($boxname) ? $boxname : '' ?>">
  <a href="/profile#savedboxes"><button id='save-name' disable>SAVE BOX</button></a>
</div>
</main>



<script src="assets/js/boxapp.js" type='text/javascript'></script>
</body>
</html>