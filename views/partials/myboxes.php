<?php




$user = $_SESSION['user'];

$boxes = $app['database']->checkIfExist('saved_boxes', 'user_id', $user['id']);
echo "<div id='mybox-container'>";
if($boxes) {
  foreach($boxes as $box) {
    if($box['status'] == 'active') {
    $pty = $app['database']->checkIfExist('box_properties', 'id', $box['property_id'])[0];
    include 'boxvariables.php';
?>
  <div class="show-box" style='background-color: <?= $frontbg ?>'>
    <?php
      $img = '';
      if($front != ''){
        $img = $front;
      }else if ($left != ''){
        $img = $left;
      }else if ($right != '') {
        $img = $right;
      }else if ($back != ''){
        $img = $back;
      }else if ($top != '') {
        $img = $top;
      }else if ($bot != '') {
        $img = $bot;
      } 
    
      echo $img == '' ? "" : "<img src='$img' alt='box-image'>";
    ?>
      <h2 class='boxname'><?= $boxname ?></h2>
      <div class="buttons">      
      <div class="box-info"><span class='boxtype'><?=$boxtype ?> Box</span></div>
      <div class='box-info'><span>Unit Price:</span> $<?= $unitprice ?></div>
      <div class='box-info'><span>Quantity:</span> <?= $qty ?></div>
      <div class='box-info'><span>Total Price:</span> $<?= $qty*$unitprice ?></div>
      <a href="customize?index=<?= $index ?>"><button id="customize" class='box-btn'>Customize</button></a>
      <a href="delete?index=<?= $index.'&pty_id='.$pty['id'] ?>"><button id="delete" class='box-btn'>Delete</button></a>
      <a href="add_to_cart?index=<?= $index ?>"><button id="add-cart" class='box-btn'>Add to Cart</button></a>
      </div>
  </div>

<?php
    }// end if
  }// foreach END
  ?>
  
  <a  href='/boxapp?<?php unset($_SESSION['box']) ?>'><button class='fixed-btn' >Create New Box</button></a>";
<?php
}else {
?>
  <div id="no-box">
    <div class="wrap">
      <div class="one side"></div>
      <div class="two side"><div class="gold"></div></div>
      <div class="three side"></div>
      <div class="four side"></div>
      <div class="five side"></div>
      <div class="six side"></div>
    </div>  
    <div class='nobox-text'>
      <h2>Sorry, we can't find your box mate :(</h2>
    </div>
    <a href="/boxapp"><button class='go-to-app'>Try it now!</button></a>
    
  </div>



<?php
} // if($boxes) END
echo "</div>";