<?php

session_start();
$box_num = 0;
echo "  <div id='cart-container'>";
if(isset($_SESSION['cart'])) {
  foreach($_SESSION['cart'] as $key => $value) {
    $box = $_SESSION['cart'][$key];
    $pty = $_SESSION['cpty'][$key];
    include 'boxvariables.php';
    extract($box);
    extract($pty);
    $index = $key;
    $totalprice = $quantity * $unit_price;

    
    $box_num++;

    

?>


    <div class="cart-item">
    <div class="box_num"><?= $box_num ?></div>
    <div id="perspective">
      <div class="box-wrapper shipping-box" id='front-anim'>

        <box class="front active" style='background-color: <?= $frontbg ?>'>
        <?= $front ? "<img src='$front' style='$frontcss' class='face-img'>"  : '' ?>
        </box>
        <box class="left" style='background-color: <?= $leftbg ?>'>
        <?= $left ? "<img src='$left' style='$leftcss' class='face-img'>"  : '' ?>
        </box>
        <box class="right" style='background-color: <?= $rightbg ?>'>
        <?= $right ? "<img src='$right' style='$rightcss' class='face-img'>"  : '' ?>
        </box>
        <box class="back" style='background-color: <?= $backbg ?>'>
        <?= $back ? "<img src='$back' style='$backcss' class='face-img'>"  : '' ?>
        </box>
        <box class="top" style='background-color: <?= $topbg ?>'>
        <?= $top ? "<img src='$top' style='$topcss' class='face-img'>"  : '' ?>
        </box>
        <box class="bottom" style='background-color: <?= $bottombg ?>'>
      <?= $bot ? "<img src='$bot' style='$bottomcss' class='face-img'>"  : '' ?> 
        </box>

      </div>
    </div>
 
     
      <div class="details">
        <div class="qty">Qty<span><?= $quantity ?></span></div>
        <div class="price">Unit Price<span>$<?= $unit_price ?></span></div>
        <div class='total-price'>Total<span>$<?= $totalprice ?></span></div>
      </div>
        <form class='total-form' action="/checkout?index=<?=$index?>" method="POST" >
          <input type="number" name="total" value='<?=$totalprice?>'  class='input-total'>
          <button type='submit' class='checkout'>checkout</button>
        </form>

</div>

    
<?php
  }
}
echo "  </div>";