<?php

session_start();

$user = $_SESSION['user'];

?> 
 
    <form action="/address" method='POST'>
      <h2 class='contact-h2'>Contact Information</h2>
      <label for="firstname">First Name <span class='star'>*</span></label>
      <input type="text" name="firstname" id="firstname" value='<?= $user['firstName']?>'>
      <label for="lastname">Last Name <span class="star">*</span></label>
      <input type="text" name="lastname" id="lastname" value='<?= $user['lastName']?>'>
      <label for="email">Email</label>
      <input type="email" name="email" id="email" value='<?= $user['email']?>'>
      <label for="phone">Phone Number <span class="star">*</span></label>
      <input type="number" name="phone" id="phone">
      <h2 class="contact-h2">Address</h2>
      <label for="address">Street Address <span class="star">*</span></label>
      <input type="text" name="address" id="address">
      <label for="city">City <span class="star">*</span></label>
      <input type="text" name="city" id="city">
      <label for="state">State/Province <span class="star">*</span></label>
      <input type="text" name="state" id="state">
      <label for="zipcode">Zip/Postal Code <span class="star">*</span></label>
      <input type="number" name="zipcode" id="zipcode">
      <label for="country">Country <span class="star">*</span></label>
      <input type="text" name="country" id="country">
      <button type="submit" id='update'>Update</button>
    </form>




