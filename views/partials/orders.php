<?php


$username = $_SESSION['user']['username'];

echo "<div id='order-container'>";
if($username != 'admin') :
  $orders = $app['database']->checkIfExist('orders', 'user_id', $_SESSION['user']['id']);
  array_map( function($order) {
    userOrders($order);
  }, array_reverse($orders,0));
else : 

  $orders = $app['database']->checkIfExist('orders', 'status', 'pending');
  
  array_map(function($order){
    allOrders($order);
  }, array_reverse($orders,0));
?>
<?php endif; ?>



<?php

echo "</div>";
//show all order (user)
function  userOrders($order) {
  extract($order);
?>
<div class="order-item">
  <div class="order-number">Order#<?=$id?></div>
  <div class="order-total">Total: <span>$<?=$total?></span></div>
  <div class="order-date">Ordered date: <span><?=$ordered_date?></span></div>
  <div class="order-status">Status: <span style="background-color: <?= $status == 'pending' ? '#F05D4E' : '#7bb501'; ?>" class='span-status'><?=$status?></span></div>
  <?= $received_date ? "<div class='order-receive'>$received_date</div>" : '' ?>
  <button class='order-summary' data-index='<?=$box_id?>'><i class='fa fa-plus-circle'> </i> Summary</button>
  <a href="/delete-order?id=<?=$order['id']?>"><button class="delete-order">X</button></a>
</div>
<div id='summary-container'>

</div>

<?php
}// end of userOrders()

//show all orders for (admin)
function allOrders($order) {
  extract($order);

?>

<div class="order-item">
  <div class="order-number">Order#<?=$id?></div>
  <?= $received_date ? "<div class='order-receive'>$received_date</div>" : '' ?>
  <div class="order-total">Total: <span>$<?=$total?></span></div>
  <div class="order-date">Ordered date: <span><?=$ordered_date?></span></div>
  <div class="order-status">Status: <span style="background-color: <?= $status == 'pending' ? '#F05D4E' : '#7bb501'; ?>" class='span-status'><?=$status?></span></div>
  <button class='order-summary' data-index='<?=$box_id?>'><i class='fa fa-plus-circle'> </i> Summary</button>
  <button class='deliver' data-index='<?=$id?>'>Process</button>
</div>
<div id='summary-container'>

</div>

<?php
}// allOrders END

?>


<script>
$('.order-summary').on('click', function() {
  var index = $(this).data('index');
  $.ajax({
    metod: 'GET',
    url: '/summary',
    data:{
      index: index
    },
    success: function(data) {
      $('#summary-container').html(data);
    }
  })
});

$('.deliver').on('click', function() {
  var index = $(this).data('index');
  $.ajax({
    method: 'GET',
    url: '/proccess-order',
    data: {
      index: index
    },
    success: function(data) {
      
    }
  })
  location.reload(true);
});
</script>