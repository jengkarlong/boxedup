<h2 style='color: #383838'>Create an account</h2>
<form action="/register" method='POST' id='regform'>
  <input type="text" name="firstname" id="firstname" placeholder='First Name' required>
  <input type="text" name="lastname" id="lastname" placeholder='Last Name' required><br>
  <input type="email" name='email' id='email' placeholder='Email Address' required>
  <input type="text" name="username" id="username" placeholder='Username' required><br>
  <input type="password" name="pwd" id='pwd' placeholder='Password' required>
  <input type="password" name="cpwd" id='cpwd' placeholder='Confirm Password' required><br>
  <input type="submit" id='reg-new-user' value="Register" disabled>
</form>

<script type='text/javascript'>
// Create Account Validation
$('#pwd').on('input', function() {
var passval = $(this).val();
  if (passval.length >= 6 && passval.indexOf(' ') < 0) {
    $('#reg-new-user').attr('disabled', false);
    $(this).css('border-color', '#00FE1E');
  }else{
    $('#reg-new-user').attr('disabled', true)
    $(this).css('border-color', 'red');
  }
});
$('#cpwd').on('input', function() {
  var passval = $('#pwd').val(),
      thisval = $(this).val();

  if(passval == thisval) {
    $('#reg-new-user').attr('disabled', false);
    $(this).css('border-color', '#00FE1E');
  } else {
    $('#reg-new-user').attr('disabled', true)
    $(this).css('border-color', 'red');
  }
});

//check if username is valid
$('#username').on('input', function() {
  var userval = $(this).val();
  $.ajax({
    url: 'validate.php',
    method: 'POST',
    data: {
      user: userval
    },
    success: function(data) {
      if(data == 'invalid' || userval.length == 0 || userval.indexOf(' ') >= 0) {
        $('#username').css('border-color', 'red');
         $('#reg-new-user').attr('disabled', true);
      }else if (data == 'valid' ) {
        $('#username').css('border-color','#00FE1E');
        $('#reg-new-user').attr('disabled', false);
      }
    }
  });
});

//check email if valid
$('#email').on('input', function() {
  var userval = $(this).val(),
      type = $(this).attr('name');
  $.ajax({
    url: 'validate.php',
    method: 'POST',
    data: {
      email: userval
    },
    success: function(data) {
      if(data == 'invalid' || userval.length == 0) {
        $('#email').css('border-color', 'red');
         $('#reg-new-user').attr('disabled', true);
      }else if (data == 'valid' ) {
        $('#email').css('border-color','#00FE1E');
        $('#reg-new-user').attr('disabled', false);
      }
    }
  });
});

//valid as long as input is not empty
$('#firstname, #lastname').on('input', function() {
 var userval = $(this).val();

if( userval.length > 0) {
    $(this).css('border-color','#00FE1E');
    $('#reg-new-user').attr('disabled', false);
} else {
    $(this).css('border-color','red');
    $('#reg-new-user').attr('disabled', true);
}

});

</script>