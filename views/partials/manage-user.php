<?php

$users = $app['database']->selectAll('users');
echo "<div id='user-container'>";
array_map(function($user){
  showUser($user);
}, $users);

echo "</div>";

function showUser($user)
{
  extract($user);
?>
<div class="user-item">
  <div class="name">Name: <span><?= $firstName.' '.$lastName?></span></div>
  <div class="username">Username: <span><?= $username?></span></div>
  <div class="email">Email: <span><?=$email?></span></div>
  <a href="/deactivate-user?id=<?=$user['id']?>"><button class="status" style='background-color:  <?= $status != 'active' ? '#F05D4E' : '#7bb501'; ?>'><?=$status?></button></a>
</div>
<script>
  $('.status').on('mouseover', function() {
    if($(this).html() == 'active')
      $(this).html('deactivate');
    else
      $(this).html('active');
  });
  $('.status').on('mouseout', function() {
    if($(this).html() == 'active')
      $(this).html('deactivate');
    else
      $(this).html('active');
  });

  $('.status').on('click', function() {
    

  });
</script>
<?php
}


