<?php


if(!isset($_SESSION['user'])) {
  header('location: /#register');
}
require 'partials/get.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Boxedup | Account</title>
  <?php get_head(); ?>
  <link rel="stylesheet" href="assets/css/profile.css">
</head>
<body>
  <?php get_header(); ?>
<main>
  <nav id="profile-nav">
    <ul>
      <?php if($_SESSION['user']['username'] != 'admin') :?>
      <li id='myboxes'><a href="/profile#savedboxes">My boxes</a></li>
      <li id='see-cart'><a href="/profile#cart">Cart</a></li>
      <li id='order-status'><a href="/profile#orders">Order</li></a>
      <li id='details' class='selected'><a href="/profile">Account</a></li>
      <?php else: ?>
      <li id="order-status"><a href="/profile#orders">Orders</a></li>
      <li id="manage-users"><a href="/profile#users">Manage Users</a></li>
      <li id="details" class='selected'><a href="/profile">Account</a></li>
      <?php endif; ?>
      
    </ul>
  </nav>

  <section id="main-view">

  </section>


</main>

 <?php get_footer(); ?>
 <script src="assets/js/profile.js"></script>
</body>
</html>