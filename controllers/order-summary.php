<?php 


$boxid = $_GET['index'];

$box = $app['database']->checkIfExist('saved_boxes', 'id', $boxid)[0];
$pty = $app['database']->checkIfExist('box_properties', 'id', $box['property_id'])[0];
$user = $app['database']->checkIfExist('users', 'id', $box['user_id'])[0];
extract($user);
extract($box);
extract($pty);
$img_count = 0;

$front_img ? $img_count++ : $img_count;
$left_img ? $img_count++ : $img_count;
$right_img ? $img_count++ : $img_count;
$back_img ? $img_count++ : $img_count;
$bottom_img ? $img_count++ : $img_count;
$top_img ? $img_count++ : $img_count;


?>

<div class="box-summary">
  <h3>Order Summary</h3>
  <div>Box name: <span><?=$box_name?></span></div>
  <div>Owner: <span><?= $firstName.' '.$lastName?></span></div>
  <div>Quantity: <span><?= $quantity ?></span></div>
  <ul>
    <strong>Background</strong>  
    <li>Front: <span><?=$front_bg?></span></li>
    <li>Left: <span><?=$left_bg?></span></li>
    <li>Right: <span><?=$right_bg?></span></li>
    <li>Back: <span><?=$back_bg?></span></li>
    <li>Top: <span><?=$top_bg?></span></li>
    <li>Bottom: <span><?=$bottom_bg?></span></li>
  </ul>
  <div>Box image count: <span><?=$img_count?></span></div>
  <button class="close-summary">Close</button>
</div>

<script type='text/javascript'>
  $('.close-summary').on('click', function() {
    $(this).parent().remove();
  });
</script>