<?php



$json = $_GET['json'];
$box = json_decode($json, true);
$user_id =$_SESSION['user']['id'];

//check if exist
$boxname = $box['boxname'];
$boxtype = $box['boxType'];
$qty = $box['qty'];
$unitprice = $box['unitPrice'];
$frontimg = $box['frontImage'] ? implode(',', $box['frontImage']) : '';
$backimg = $box['backImage'] ? implode(',', $box['backImage']) : '';
$leftimg = $box['leftImage'] ? implode(',', $box['leftImage']) : '';
$rightimg = $box['rightImage'] ? implode(',', $box['rightImage']) : '';
$topimg = $box['topImage'] ? implode(',', $box['topImage']) : '';
$bottomimg = $box['bottomImage'] ? implode(',', $box['bottomImage']) : '';
$frontbg = $box['frontBG'];
$leftbg = $box['leftBG'];
$rightbg = $box['rightBG'];
$backbg = $box['backBG'];
$topbg = $box['topBG'];
$bottombg = $box['bottomBG'];


$find = $app['database']->fetchBox($user_id, $boxname);
if($find) {
  // Rewrite existing box
  $pty_id = $find[0]['property_id'];
  $app['database']->boxProperties('update', $pty_id,$boxname, $frontimg, $leftimg, $rightimg, $backimg, $topimg, $bottomimg, $frontbg, $leftbg, $rightbg, $backbg, $topbg, $bottombg);
  $app['database']->saveNewBox('update', '', $user_id, $boxname, $qty, $unitprice, $boxtype);
}else {
  $app['database']->boxProperties('insert', '', $boxname, $frontimg, $leftimg, $rightimg, $backimg, $topimg, $bottomimg, $frontbg, $leftbg, $rightbg, $backbg, $topbg, $bottombg);
  $property = $app['database']->selectAll('box_properties');
  $pty_id;
  array_map(function($array) {
    global $boxname;
    global $pty_id;
    $pty_id = $array['box_name'] == $boxname ? $array['id'] : '';
  }, $property);

  if($pty_id != '') {
    $app['database']->saveNewBox('insert',$pty_id, $user_id, $boxname, $qty, $unitprice, $boxtype);
  }
}






// print_r($frontimage);