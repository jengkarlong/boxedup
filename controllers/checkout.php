<?php


date_default_timezone_set('Asia/Manila');
$boxid = $_GET['index'];
$total = $_POST['total'];
$date = date('F j Y h:i:s A');
$user_id = $_SESSION['user']['id']; // user(id)
$box = $app['database']->checkIfExist('saved_boxes', 'id', $boxid)[0];
extract($box);
unset($_SESSION['cart'][$boxid]); // remove target box from the cart

$app['database']->modifyOrders('insert', $user_id, $boxid, $quantity, $unit_price, $total, $date);

header('location: profile#cart');

