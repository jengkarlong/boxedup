<?php

require 'core/bootstrap.php';

if(isset($_POST['user'])){
  $username = $_POST['user'];
  $usercheck = $app['database']->checkIfExist('users', 'username', $username);

  if($usercheck) {
    echo 'invalid';
  }else {
    echo 'valid';
  }
}
if (isset($_POST['email'])) {
  $email = $_POST['email'];
  $checkemail = test_input($email);
  if (!filter_var($checkemail, FILTER_VALIDATE_EMAIL)) {
    $emailErr = true; 
  }else {
    $emailfind = $app['database']->checkIfExist('users', 'email', $email);
  }
  if (!isset($emailErr) && $emailfind  ) {
    echo "invalid";
  }else if (!isset($emailErr)){
    echo "valid";
  }
}

//test email format
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}