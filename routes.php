<?php
$router->get('', 'controllers/index.php');
$router->get('boxapp', 'controllers/boxapp.php');
$router->get('profile', 'controllers/profile.php' );
$router->post('register', 'controllers/new_user.php');
$router->post('login', 'controllers/authenticate.php');
$router->get('#register', 'controllers/index.php');
$router->get('logout', 'controllers/logout.php');
$router->get('savebox', 'controllers/savebox.php');
$router->get('myboxes', 'views/partials/myboxes.php');
$router->get('delete', 'controllers/deletebox.php');
$router->get('customize', 'controllers/customize.php');
$router->get('add_to_cart', 'controllers/add_to_cart.php');
$router->get('checkout', 'controllers/checkout.php');
$router->post('checkout', 'controllers/checkout.php');
$router->get('orders', 'views/partials/orders.php');
$router->get('summary', 'controllers/order-summary.php');
$router->get('proccess-order', 'controllers/proccess-order.php');
$router->get('manage-user', 'views/partials/manage-user.php');
$router->get('delete-order', 'controllers/delete-order.php');
$router->get('deactivate-user', 'controllers/deactivate-user.php');